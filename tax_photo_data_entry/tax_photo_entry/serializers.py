from rest_framework import serializers
from tax_photo_entry.models import Photo_Metadata, Dublin_Core


class Photo_MetadataSerializer(serializers.ModelSerializer):
	class Meta:
		model = Photo_Metadata

	
		fields = ('auto_photo_id', 'photo_identifier', 'borough', 'block', 'lot', 'building_number', 'street_name', 'zip_code', 'landmark_name','lot_frontage',
			 'lot_depth', 'year_built', 'year_altered_one', 'year_altered_two', 'date', 'marked_delete', 'coverage_name', 'description', 'notes')




class DC_MetadataSerializer(serializers.ModelSerializer):

	class Meta:
		model = Dublin_Core

		fields = ('photo', 'borough', 'block', 'lot', 'zip_code', 'landmark_name','title_alternative', 'coverage_temporal', 'start', 'end', 'building_number',
		'street_name', 'city', 'coverage_name',
			'coverage_latitude_north', 'coverage_longitude_east', 'abstract', 'description', 'type_property', 'relation', 'is_format_of',
			'source', 'subject', 'contributor', 'creator', 'publisher', 'rights', 'date_created', 'resource_format', 'extent', 'medium',
			'photo_identifier', 'accrual_method', 'accrual_periodicity', 'accraul_policy', 'provenance')

	

