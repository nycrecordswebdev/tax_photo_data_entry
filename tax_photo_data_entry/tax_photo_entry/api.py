from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpRequest
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from tax_photo_entry.models import Photo_Metadata, Dublin_Core
from tax_photo_entry.serializers import Photo_MetadataSerializer, DC_MetadataSerializer
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response 
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
import requests
import json
from helper_functions import process_file
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from rest_framework.authentication import TokenAuthentication
from tax_photo_entry.authenticators import UserAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from .forms import LotForm, EditData
from .forms import UploadFileForm
from rest_framework.authentication import SessionAuthentication, BasicAuthentication






@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def filter_photos(request, borough, block_num, lot_num, building_number, street_name, zip_code, format=None):
	kwargs = {}
	if borough != 'NA':
		kwargs['borough'] = borough
	if block_num != 'NA':
		kwargs['block'] = block_num
	if lot_num != 'NA':
		kwargs['lot'] = lot_num
	if street_name != 'NA':
		kwargs['street_name'] = street_name
	if zip_code != 'NA':
		kwargs['zip_code'] = zip_code
	if building_number != 'NA':
		kwargs['building_number'] = building_number
	try:
		photo = Photo_Metadata.objects.filter(**kwargs)
	except Photo_Metadata.DoesNotExist:
		return Response(status = status.HTTP_404_NOT_FOUND)	
	serializer = Photo_MetadataSerializer(photo, many = True)
	return Response(serializer.data)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def filter_dublin_core(request, borough, block_num, lot_num, building_number, street_name, zip_code, format=None):
	kwargs = {}
	if borough != 'NA':
		kwargs['borough'] = borough
	if block_num != 'NA':
		kwargs['block'] = block_num
	if lot_num != 'NA':
		kwargs['lot'] = lot_num
	if street_name != 'NA':
		kwargs['street_name'] = street_name
	if zip_code != 'NA':
		kwargs['zip_code'] = zip_code
	if building_number != 'NA':
		kwargs['building_number'] = building_number	
	try:
		photo = Dublin_Core.objects.filter(**kwargs)	
	except Dublin_Core.DoesNotExist:
		return Response(status = status.HTTP_404_NOT_FOUND)
	serializer = DC_MetadataSerializer(photo, many = True)
	return Response(serializer.data)


@api_view(['PUT'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def update_single_photo(request, entry_id, format=None):
	try:
		photo = Photo_Metadata.objects.filter(auto_photo_id=entry_id)
	except Photo_Metadata.DoesNotExist:
		return Response(status = status.HTTP_404_NOT_FOUND)
	data = request.data
	serializer = Photo_MetadataSerializer(photo[0], data = data)
	if serializer.is_valid():
		serializer.save()
		return Response(serializer.data)
	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def get_a_photo(request, borough, block, lot, format=None):
	try:
		photo = Photo_Metadata.objects.filter(borough=borough, block=block, lot=lot)
	except Photo_Metadata.DoesNotExist:
		return Response(status = status.HTTP_404_NOT_FOUND)
	serializer = Photo_MetadataSerializer(photo, many = True)
	return Response(serializer.data)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def get_photo_w_id(request, photo_id, format=None):
	try:
		photo = Photo_Metadata.objects.filter(auto_photo_id=photo_id)
	except Photo_Metadata.DoesNotExist:
		return Response(status = status.HTTP_404_NOT_FOUND)
	serializer = Photo_MetadataSerializer(photo, many = True)
	return Response(serializer.data)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def get_all_dublin_core(request, format=None):
	#if request.method == 'GET':
	dublin_core = Dublin_Core.objects.all()
	serializer = DC_MetadataSerializer(photos, many=True)
	return Response(serializer.data)

	
@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def post_new_dublin_core(request, format=None):
	serializer = DC_MetadataSerializer(data=request.data)
	if serializer.is_valid():
		serializer.save()
		return Response(serializer.data, status=status.HTTP_201_CREATED)
	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def get_single_dc_data(request, photo, format=None):
	try:
		dublin_core = Dublin_Core.objects.filter(photo = int(photo))
	except Dublin_Core.DoesNotExist:
		return Response(status = status.HTTP_404_NOT_FOUND)
	serializer = DC_MetadataSerializer(dublin_core, many = True)
	return Response(serializer.data)



@api_view(['DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def delete_single_dc_data(request, photo_id, format=None):
	try:
		dublin_core = Dublin_Core.objects.filter(photo = photo_id)
	except Dublin_Core.DoesNotExist:
		return Response(status = status.HTTP_404_NOT_FOUND)
	dublin_core.delete()
	return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def delete_single_photo_data(request, photo_id, format=None):
	try:
		photo = Photo_Metadata.objects.filter(auto_photo_id = photo_id)
	except Photo_Metadata.DoesNotExist:
		return Response(status = status.HTTP_404_NOT_FOUND)
	photo.delete()
	return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['PUT'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def update_single_dc_data(request, photo_id, format=None):
	try:
		dublin_core = Dublin_Core.objects.filter(photo = photo_id)
	except Dublin_Core.DoesNotExist:
		return Response(status = status.HTTP_404_NOT_FOUND)
	data = request.data
	serializer = DC_MetadataSerializer(dublin_core[0], data = data)
	if serializer.is_valid():
		serializer.save()
		return Response(serializer.data)
	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
   

@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def photo_list(request, format=None):
	if request.method == 'GET':
		photos = Photo_Metadata.objects.all()
		serializer = Photo_MetadataSerializer(photos, many=True)
		return Response(serializer.data)
	elif request.method == 'POST':
		serializer = Photo_MetadataSerializer(data = request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status = status.HTTP_201_CREATED)
		return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)



