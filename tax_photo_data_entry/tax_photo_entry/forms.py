from django import forms


class LotForm(forms.Form):
    borough = forms.CharField(label='BOROUGH', widget=forms.TextInput(attrs={'class': 'myfieldclass'}))
    block = forms.CharField(required=False, label='BLOCK NUMBER')
    lot = forms.CharField(required=False, label='LOT NUMBER')
    building_number = forms.CharField(required=False, label='BUILDING NUMBER')
    street_name = forms.CharField(required=False, label='STREET NAME')
    zip_code = forms.CharField(required=False, label='ZIP CODE')


class CreateNew(forms.Form):

    photo_identifier = forms.CharField(label='PHOTO IDENTIFIER', required=True, error_messages={'required': 'This field is required'})
    borough = forms.CharField(label='BOROUGH', required=True, error_messages={'required': 'This field is required'})
    block = forms.CharField(label='BLOCK', required=True, error_messages={'required': 'This field is required'})
    lot = forms.CharField(label='LOT', required=True, error_messages={'required': 'This field is required'})
    building_number = forms.CharField(label='BUILDING NUMBER', required=True, error_messages={'required': 'This field is required'})
    street_name = forms.CharField(label='STREET NAME', required=True, error_messages={'required': 'This field is required'})
    zip_code = forms.CharField(label='ZIP CODE', required=True, error_messages={'required': 'This field is required'})
    lot_frontage = forms.CharField(label='LOT FRONTAGE', required=True, error_messages={'required': 'This field is required'})
    lot_depth = forms.CharField(label='LOT DEPTH', required=True, error_messages={'required': 'This field is required'})
    year_built = forms.CharField(label='YEAR BUILT', required=True, error_messages={'required': 'This field is required'})
    year_altered_one = forms.CharField(label='YEAR ALTERED ONE', required=True, error_messages={'required': 'This field is required'})
    year_altered_two = forms.CharField(label='YEAR ALTERED TWO', required=True, error_messages={'required': 'This field is required'})
    date = forms.CharField(label='DATE', required=True, error_messages={'required': 'This field is required'})
    coverage_name = forms.CharField(label='COVERAGE NAME', required=True, error_messages={'required': 'This field is required'})
    description = forms.CharField(label='DESCRIPTION', required=True, error_messages={'required': 'This field is required'})
    notes = forms.CharField(label='NOTES', required=True, error_messages={'required': 'This field is required'})



class EditData(forms.Form):
    title_borough = forms.CharField(label='BOROUGH')
    title_block = forms.CharField(label='BLOCK')
    title_lot = forms.CharField(label='LOT')
    description = forms.CharField(label='DESCRIPTION')
    # is_format_of = forms.CharField(label = 'is_format_of')
    coverage_name = forms.CharField(label='COVERAGE NAME')
    coverage_temporal = forms.CharField(label='ORIGINAL DATE')
    subject = forms.CharField(label='SUBJECT')

    coverage_spatial_building = forms.CharField(label='BUILDING NUMBER')
    coverage_spatial_street = forms.CharField(label='STREET NAME')
    coverage_spatial_city = forms.CharField(label='CITY')


class UploadFileForm(forms.Form):
    file = forms.FileField()
