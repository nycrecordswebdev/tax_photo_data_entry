from tax_photo_entry.models import Photo_Metadata, Dublin_Core
import requests
import csv


metadata_defaults = {"title_alternative": "New York City Tax Photograph Collection", "temporal_coverage": "1939-1941", "temporal_start": "1939", "temporal_end":"1941",
	"abstract": "The New York City Tax Photograph Collection documents nearly every building within Manhattan, Queens, Brooklyn, Bronx, and Staten Island between 1939- 1940.", 
	"type": "Stillimage", "isformatof":"35mm nitrate negative and photographic prints", "source": "Metadata is in part derived from the NYC Department of Finance's Real Property Assessment Data (RPAD).", "contributor":"New York (N.Y.) Municipal Archives", "publisher":"New York(N.Y.) Dept. of Records and Information Services, Municipal Archives/31 Chambers St., Room 101, New York, New York, United States 10007", "rights": "The Municipal Archives does not determine the copyright status  on materials, instead copyright permissions are the responsibility of the researcher.  The Municipal Archives requests full and proper credit to cite sources:   Courtesy of NYC Municipal Archives.",
	"format": "image/tiff", "language":"eng"}


def process_file(f):

	reader = csv.reader(f)
	reader.next()

	for row in reader:
		c = Photo_Metadata(photo_identifier = row[0],
				borough = row[1], block = int(float(row[2])), lot = int(float(row[3])),
				building_number = row[4], street_name = row[5],
				zip_code = int(float(row[6])), lot_frontage = row[7], lot_depth = row[8],
				year_built = row[9], year_altered_one = row[10],
				year_altered_two = row[11], date=row[12])
		c.save()
		d = Dublin_Core(photo = c.auto_photo_id, title_borough = c.borough, title_block = c.block, title_lot = c.lot, title_alternative = metadata_defaults["title_alternative"], coverage_temporal = metadata_defaults["temporal_coverage"],
			start = metadata_defaults["temporal_start"], end = metadata_defaults["temporal_end"], coverage_spatial_building =c.building_number,
			coverage_spatial_street=c.street_name, coverage_spatial_city=getAddress(c), abstract = metadata_defaults["abstract"],
			type_property = metadata_defaults["type"], is_format_of = metadata_defaults["isformatof"], source = metadata_defaults["source"],
			contributor = metadata_defaults["contributor"], publisher = metadata_defaults["publisher"], rights = metadata_defaults["rights"],
			resource_format = metadata_defaults["format"], resource_identifier = getIdentifier(c), accraul_policy = 'blank', zip_code=c.zip_code)
		d.save()
	


def loadDublinCore():
	photos = Photo_Metadata.objects.all()
	for p in photos:
		

		d = Dublin_Core(photo = p, title = getTitle(p), title_alternative = metadata_defaults["title_alternative"], coverage_temporal = metadata_defaults["temporal_coverage"],
			start = metadata_defaults["temporal_start"], end = metadata_defaults["temporal_end"], coverage_spatial = getCoverageSpatial(p), abstract = metadata_defaults["abstract"],
			type_property = metadata_defaults["type"], is_format_of = metadata_defaults["isformatof"], source = metadata_defaults["source"],
			contributor = metadata_defaults["contributor"], publisher = metadata_defaults["publisher"], rights = metadata_defaults["rights"],
			resource_format = metadata_defaults["format"], resource_identifier = getIdentifier(p), accraul_policy = 'blank')
		d.save()


def getAddress(photo):
	if photo.borough == 'Manhattan':
		city = 'New York'
	else: 
		city = photo.borough
	state = 'New York'
	zip_code = photo.zip_code
	address = city + ', ' + state + ', ' + str(zip_code)
	return address

def generateDublinCore():
	photos = Photo_Metadata.objects.all()
	for p in photos:
		print getTitle(p)
		print getCoverageSpatial(p)
		print getIdentifier(p)

def getTitle(photo):
	borough = photo.borough
	block = photo.block
	lot = photo.lot
	title = str(block) + ", " + str(lot) + ", "  + borough 
	return title 

def getCoverageSpatial(photo):
	building_number = photo.building_number
	street_name = photo.street_name
	city = "New York"
	state = "NY"
	zip_code = photo.zip_code
	spatial_coverage = str(building_number) + ", " + str(street_name) + ", " + city + ", " + state + ", " + str(zip_code)
	return spatial_coverage

def getIdentifier(photo):
	return photo.photo_identifier