from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import Permission

class Photo_Metadata(models.Model):   
    auto_photo_id = models.AutoField(primary_key=True)
    photo_identifier = models.CharField(blank=True, null=True, max_length=50)
    borough = models.CharField(max_length=15, blank=True, null=True)
    block = models.CharField(max_length=10, blank=True, null=True)
    lot = models.CharField(max_length=10, blank=True, null=True)
    building_number = models.CharField(max_length=15, blank=True, null=True)
    street_name = models.CharField(max_length=50, blank=True, null=True)
    zip_code = models.CharField(max_length=12, blank=True, null=True)
    landmark_name = models.CharField(max_length=500, blank=True, null=True)
    lot_frontage = models.CharField(max_length=15, blank=True, null=True)
    lot_depth = models.CharField(max_length=15, blank=True, null=True)
    year_built = models.CharField(max_length=20, blank=True, null=True)
    year_altered_one = models.CharField(max_length=20, blank=True, null=True)
    year_altered_two = models.CharField(max_length=20, blank=True, null=True)
    date = models.CharField(max_length=10, blank=True, null=True)
    marked_delete = models.BooleanField(blank=True, default=False)
    coverage_name = models.CharField(max_length=500, blank=True, null=True)
    description = models.CharField(max_length=500, blank=True, null=True)
    notes = models.CharField(max_length=500, blank=True, null=True)
    
    class  Meta:
        permissions = (
            ("can_confirm_deletion", "Can delete an entry that's been marked for deletion"),
        )


class Dublin_Core(models.Model):
    photo = models.IntegerField(blank=True, null=True)
    borough = models.CharField(max_length=15, blank=True, null=True)
    block = models.CharField(max_length=10, blank=True, null=True)
    lot = models.CharField(max_length=10, blank=True, null=True)
    title = models.CharField(max_length=100, blank=True, null=True)
    title_alternative = models.CharField(max_length=100, blank=True, null=True)
    coverage_temporal = models.CharField(max_length=25, blank=True, null=True)
    start = models.IntegerField(blank=True, null=True)
    end = models.IntegerField(blank=True, null=True)
    building_number = models.CharField(max_length=150, blank=True, null=True)
    street_name = models.CharField(max_length=150, blank=True, null=True)
    city = models.CharField(max_length=150, blank=True, null=True)
    zip_code = models.CharField(max_length=12, blank=True, null=True)
    landmark_name = models.CharField(max_length=500, blank=True, null=True)
    coverage_name = models.CharField(max_length=500, blank=True, null=True)
    coverage_latitude_north = models.CharField(max_length=10, blank=True, null=True)
    coverage_longitude_east = models.CharField(max_length=10, blank=True, null=True)
    abstract = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=500, blank=True, null=True)
    type_property = models.CharField(max_length=255, blank=True, null=True)
    relation = models.CharField(max_length=255, blank=True, null=True)
    is_format_of = models.CharField(max_length=255, blank=True, null=True)
    source = models.CharField(max_length=255, blank=True, null=True)
    subject = models.CharField(max_length=255, blank=True, null=True)
    contributor = models.CharField(max_length=50, blank=True, null=True)
    creator = models.CharField(max_length=100, blank=True, null=True)
    publisher = models.CharField(max_length=500, blank=True, null=True)
    rights = models.CharField(max_length=500, blank=True, null=True)
    date_created = models.CharField(max_length=15, blank=True, null=True)
    resource_format = models.CharField(max_length=15, blank=True, null=True)
    extent = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    medium = models.CharField(max_length=255, blank=True, null=True)
    photo_identifier = models.CharField(max_length=50, blank=True, null=True)
    accrual_method = models.CharField(max_length=255, blank=True, null=True)
    accrual_periodicity = models.CharField(max_length=255, blank=True, null=True)
    accraul_policy = models.CharField(max_length=255, blank=True, null=True)
    provenance = models.CharField(max_length=100, blank=True, null=True)




