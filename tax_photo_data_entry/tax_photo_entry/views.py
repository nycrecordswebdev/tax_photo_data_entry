from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpRequest
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from tax_photo_entry.models import Photo_Metadata, Dublin_Core
from tax_photo_entry.serializers import Photo_MetadataSerializer, DC_MetadataSerializer
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
import requests
import json
from helper_functions import process_file
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from rest_framework.authentication import TokenAuthentication
from tax_photo_entry.authenticators import UserAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from .forms import LotForm, EditData, CreateNew
from .forms import UploadFileForm
from django.shortcuts import render
from tax_photo_entry.models import Photo_Metadata

# assumptions:
# a user should be able to search the database for entries with a certain lot and block number
# a user should be able to edit certain fields within that entry

headers = {'content_type': 'application/json',
           'Authorization': 'Token dc62f54d8c9a17307498975aec178d0a347d46d4'}


def marked_for_delete(request):
    if not request.user.has_perm('tax_photo_entry.can_confirm_deletion'):
        return render(request, 'tax_photo_entry/not_authorized.html')
    if request.user.has_perm('tax_photo_entry.can_confirm_deletion'):
        to_delete = Photo_Metadata.objects.filter(marked_delete=True)
    context = {'to_delete': to_delete}
    return render(request, 'tax_photo_entry/to_delete.html', context)


@login_required(login_url='/tax_photo_entry/user_login/')
def welcome_view(request):
    context = ''
    if not request.user.has_perm('tax_photo_entry.can_confirm_deletion'):
        return render(request, 'tax_photo_entry/welcome.html')
    if request.user.has_perm('tax_photo_entry.can_confirm_deletion'):
        can_delete = 'true'
    context = {'can_delete': can_delete}
    return render(request, 'tax_photo_entry/welcome.html', context)


@login_required(login_url='/tax_photo_entry/user_login/')
def entry_not_found(request):
    return render(request, 'tax_photo_entry/not_found.html')


@login_required(login_url='/tax_photo_entry/user_login/')
def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            process_file(request.FILES['file'])
            return HttpResponseRedirect(reverse('upload_success'))
    else:
        form = UploadFileForm()
    return render(request, 'tax_photo_entry/upload_file.html', {'form': form})


@login_required(login_url='/tax_photo_entry/user_login/')
def upload_success(request):
    return render(request, 'tax_photo_entry/upload_success.html')


def user_logout(request):
    logout(request)
    return render(request, 'tax_photo_entry/logged_out.html')


@login_required(login_url='/tax_photo_entry/user_login/')
def edit_success(request):
    return render(request, 'tax_photo_entry/edit_success.html')


def user_login(request):
    username = request.POST.get('username', False)
    password = request.POST.get('password', False)
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect(reverse('welcome_view'))
        else:
            alert('Account not active')
        # message saying to ask admin to activate your account for you
    else:
        return render(request, 'tax_photo_entry/login.html')


@login_required(login_url='/tax_photo_entry/user_login/')
def get_lot(request):
    if request.method == 'POST':
        form = LotForm(request.POST)
        if form.is_valid():
            borough = form.cleaned_data['borough']
            block = form.cleaned_data['block']
            lot = form.cleaned_data['lot']
            building_number = form.cleaned_data['building_number']
            street_name = form.cleaned_data['street_name']
            zip_code = form.cleaned_data['zip_code']
            if not lot:
                lot = 'NA'
            if not borough:
                borough = 'NA'
            if not block:
                block = 'NA'
            if not building_number:
                building_number = 'NA'
            if not street_name:
                street_name = 'NA'
            if not zip_code:
                zip_code = 'NA'
            url = 'https://records-tax-photo.herokuapp.com/tax_photo_entry/photos/' + borough + '/' + block + '/' + lot + '/' + building_number + '/' + street_name + '/' + zip_code + '/'
            r = requests.get(url, headers=headers)
            data = r.text
            if 'photo' in r.text:
                data = json.loads(data)
                photo_identifier = data[0]['photo_identifier']
                return HttpResponseRedirect(reverse('edit_lot', kwargs={'data': photo_identifier}))
            else:
                return HttpResponseRedirect(reverse('entry_not_found'))
    else:
        form = LotForm()


    return render(request, 'tax_photo_entry/search.html', {'form': form})


metadata_defaults = {"title_alternative": "New York City Tax Photograph Collection", "temporal_coverage": "1939-1941",
                     "temporal_start": "1939", "temporal_end": "1941",
                     "abstract": "The New York City Tax Photograph Collection documents nearly every building within Manhattan, Queens, Brooklyn, Bronx, and Staten Island between 1939- 1940.",
                     "type": "Stillimage", "isformatof": "35mm nitrate negative and photographic prints",
                     "source": "Metadata is in part derived from the NYC Department of Finance's Real Property Assessment Data (RPAD).",
                     "contributor": "New York (N.Y.) Municipal Archives",
                     "publisher": "New York(N.Y.) Dept. of Records and Information Services, Municipal Archives/31 Chambers St., Room 101, New York, New York, United States 10007",
                     "rights": "The Municipal Archives does not determine the copyright status  on materials, instead copyright permissions are the responsibility of the researcher.  The Municipal Archives requests full and proper credit to cite sources:   Courtesy of NYC Municipal Archives.",
                     "format": "image/tiff", "language": "eng"}


@login_required(login_url='/tax_photo_entry/user_login/')
def create_new(request):

    if request.method == 'POST':

        form = CreateNew(request.POST)
        if form.is_valid():
            photo_identifier = form.cleaned_data['photo_identifier']
            borough = form.cleaned_data['borough']
            block = form.cleaned_data['block']
            lot = form.cleaned_data['lot']
            building_number = form.cleaned_data['building_number']
            street_name = form.cleaned_data['street_name']
            zip_code = form.cleaned_data['zip_code']
            lot_frontage = form.cleaned_data['lot_frontage']
            lot_depth = form.cleaned_data['lot_depth']
            year_built = form.cleaned_data['year_built']
            year_altered_one = form.cleaned_data['year_altered_one']
            year_altered_two = form.cleaned_data['year_altered_two']
            date = form.cleaned_data['date']
            coverage_name = form.cleaned_data['coverage_name']
            description = form.cleaned_data['description']

            photo_url = 'https://records-tax-photo.herokuapp.com/tax_photo_entry/photos/'
            dc_url = url_two = 'https://records-tax-photo.herokuapp.com/tax_photo_entry/post_new_dublin_core/'

            photo_new_info = {'photo_identifier': photo_identifier, 'lot': lot, 'block': block, 'borough': borough,
                              'building_number': building_number,
                              'zip_code': zip_code, 'street_name': street_name, 'lot_frontage': lot_frontage,
                              'lot_depth': lot_depth, 'year_built': year_built,
                              'year_altered_two': year_altered_two, year_altered_one: 'year_altered_one', 'date': date,
                              'coverage_name': coverage_name,
                              'description': description}

            photo_r = requests.post(photo_url, headers=headers, data=photo_new_info)
            photo_response = photo_r.status_code
            photo = Photo_Metadata.objects.filter(borough=borough, block=block, lot=lot)

            photo_id = photo[0].auto_photo_id

            dublin_core_new_info = {'photo': photo_id, 'title_lot': lot, 'title_block': block, 'title_borough': borough,
                                    'description': description,
                                    'coverage_name': coverage_name,
                                    'coverage_temporal': metadata_defaults['temporal_coverage'],
                                    'coverage_spatial_building': building_number, 'coverage_spatial_city': 'New York',
                                    'coverage_spatial_street': street_name,
                                    'title_alternative': metadata_defaults['title_alternative'],
                                    'start': metadata_defaults['temporal_start'], 'end': metadata_defaults['temporal_end'],
                                    'abstract': metadata_defaults['abstract'], 'type_property': metadata_defaults['type'],
                                    'is_format_of': metadata_defaults['isformatof'],
                                    'source': metadata_defaults['source'], 'contributor': metadata_defaults['contributor'],
                                    'publisher': metadata_defaults['publisher'],
                                    'rights': metadata_defaults['rights']}

            dc_r = requests.post(dc_url, headers=headers, data=dublin_core_new_info)
            dc_response = dc_r.status_code

            return HttpResponseRedirect(reverse('edit_success'))
        else:
            return render(request, 'tax_photo_entry/create_new.html', {'form': form})

    else:
        form = CreateNew()

    return render(request, 'tax_photo_entry/create_new.html', {'form': form})



@login_required(login_url='/tax_photo_entry/user_login/')
def edit_lot(request, data):

    photo_identifier = data
<<<<<<< HEAD

=======

>>>>>>> d279d86a1a3b592aa465242c123bcd3871f1f9da
    url_two = 'https://records-tax-photo.herokuapp.com/tax_photo_entry/get_single_dc_data/' + photo_identifier + '/'
    r_two = requests.get(url_two, headers=headers)


    data_two = r_two.text

    data_two = json.loads(data_two)

    if request.method == 'POST':

        form = EditData(request.POST)

        if form.is_valid():

            title_borough = form.cleaned_data['title_borough']
            title_block = form.cleaned_data['title_block']
            title_lot = form.cleaned_data['title_lot']
            description = form.cleaned_data['description']

            coverage_name = form.cleaned_data['coverage_name']

            coverage_temporal = form.cleaned_data['coverage_temporal']

            subject = form.cleaned_data['subject']
            coverage_spatial_building = form.cleaned_data['coverage_spatial_building']
            coverage_spatial_street = form.cleaned_data['coverage_spatial_street']
            coverage_spatial_city = form.cleaned_data['coverage_spatial_city']
            dc_url = 'https://records-tax-photo.herokuapp.com/tax_photo_entry/update_single_dc_data/' + photo_identifier + '/'
            dublin_core_new_info = {'title_lot': title_lot, 'title_block': title_block, 'title_borough': title_borough,
                                    'description': description,
                                    'coverage_name': coverage_name, 'coverage_temporal': coverage_temporal,
                                    'subject': subject, 'coverage_spatial_building': coverage_spatial_building,
                                    'coverage_spatial_city': coverage_spatial_city,
                                    'coverage_spatial_street': coverage_spatial_street}

            dc_r = requests.put(dc_url, headers=headers, data=dublin_core_new_info)
            dc_response = dc_r.status_code


            city_list = coverage_spatial_city.split(',')
            new_zip = city_list[2]
            photo_url = 'https://records-tax-photo.herokuapp.com/tax_photo_entry/photos/' + data_two[0]['title_borough'] + '/' + str(
                data_two[0]['title_block']) + '/' + str(data_two[0]['title_lot']) + '/'
            photo_new_info = {'photo_identifier': data_two[0]['resource_identifier'], 'lot': title_lot,
                              'block': title_block, 'borough': title_borough,
                              'building_number': coverage_spatial_building,
                              'zip_code': new_zip, 'street_name': coverage_spatial_street}
            photo_r = requests.put(photo_url, headers=headers, data=photo_new_info)
            photo_response = photo_r.status_code

            return HttpResponseRedirect(reverse('edit_success'))
        # return HttpResponseRedirect('/thanks/')

        # change data in db


    else:
        form = EditData(data_two[0])

    return render(request, 'tax_photo_entry/edit_data.html', {'form': form})
