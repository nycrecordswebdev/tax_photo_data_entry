from django.conf.urls import url

from tax_photo_entry.views import EditData
from tax_photo_entry import views, api, ajax
from rest_framework.urlpatterns import format_suffix_patterns
from django.views.decorators.csrf import csrf_exempt

from django.views.generic import TemplateView

urlpatterns = [
    #url(r'^$', EditData.as_view(), name='EditData'),
    url(r'^get_lot/$', views.get_lot, name = 'get_lot'),
    url(r'^photos/$', api.photo_list),
    url(r'^welcome_view/$', views.welcome_view, name = 'welcome_view'),
    url(r'^marked_for_delete/$', views.marked_for_delete, name = 'marked_for_delete'),
    url(r'^user_login/$', views.user_login, name = 'user_login'),
    url(r'^user_logout/$', views.user_logout, name = 'user_logout'),
    url(r'^entry_not_found/$', views.entry_not_found, name = 'entry_not_found'),
    url(r'^upload_success/$', views.upload_success, name = 'upload_success'),
    url(r'^edit_success/$', views.edit_success, name = 'edit_success'),
    url(r'^upload_file/$', views.upload_file, name = 'upload_file'),
    url(r'^photos/(?P<borough>[\w|\W]+)/(?P<block_num>[\w|\W]+)/(?P<lot_num>[\w|\W]+)/(?P<building_number>[\w|\W]+)/(?P<street_name>[\w|\W]+)/(?P<zip_code>\w+)/$', api.filter_photos),
    url(r'^filter_dublin_core/(?P<borough>[\w|\W]+)/(?P<block_num>[\w|\W]+)/(?P<lot_num>[\w|\W]+)/(?P<building_number>[\w|\W]+)/(?P<street_name>[\w|\W]+)/(?P<zip_code>\w+)/$', api.filter_dublin_core),
    url(r'^single_photos/(?P<borough>\w+)/(?P<block_num>\w+)/(?P<lot_num>\w+)/$', api.get_a_photo),
    url(r'^get_photo_w_id/(?P<photo_id>\w+)/$', api.get_photo_w_id),
    url(r'^edit_lot/(?P<data>\w+)/$', views.edit_lot, name = 'edit_lot'),
    url(r'^create_new/$', views.create_new, name = 'create_new'),
    url(r'^get_all_dublin_core/$', api.get_all_dublin_core, name='get_all_dublin_core'),
    url(r'^post_new_dublin_core/$', api.post_new_dublin_core, name = 'post_new_dublin_core'),
    url(r'^get_single_dc_data/(?P<photo>\w+)/$', api.get_single_dc_data),
    url(r'^update_single_dc_data/(?P<photo_id>\w+)/$', api.update_single_dc_data),
    url(r'^delete_single_dc_data/(?P<photo_id>\w+)/$', api.delete_single_dc_data),
    url(r'^delete_single_photo_data/(?P<photo_id>\w+)/$', api.delete_single_photo_data),
    url(r'^update_single_photo/(?P<entry_id>\w+)/$',  api.update_single_photo, name = 'update_photo' ),
    url(r'^filter/$', ajax.filter_photo_form),
    url(r'^edit/$', ajax.edit_photo_form),
    url(r'', TemplateView.as_view(template_name='tax_photo_entry/filter_search.html'), name='filter_search'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

